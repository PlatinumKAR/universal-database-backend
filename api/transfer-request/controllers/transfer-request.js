'use strict';

const { sanitizeEntity } = require("strapi-utils/lib");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */
async function create(ctx) {
    try {
        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
        if (!allow) return ctx.badRequest(message)
        const { student_id } = ctx.request.body
        if (student_id) {
            const studdata = await strapi.services.student.findOne({ id: student_id })
            const student = student_id
            const from_institution = studdata.institution
            const to_institution = principal_data.institution
            console.log(to_institution)
            console.log(principal_data.institution)
            const field_id = `${student}_${principal_data.institution.id}_${studdata.institution.id}`
            const reTransfer = await strapi.services['transfer-request'].create({ student, from_institution, to_institution, field_id })
            return sanitizeEntity(reTransfer, { model: strapi.models['transfer-request'] })
        }
        else ctx.badRequest("Please provide parameters")

    } catch (error) {
        return ctx.badRequest(error['message']);
    }
}

async function find(ctx) {
    try {
        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
        if (!allow) return ctx.badRequest(message)
        const transferQuery = await strapi.services['transfer-request'].find({ "to_institution": principal_data['institution']['id'], "_limit": -1 })
        return sanitizeEntity(transferQuery, { model: strapi.models['transfer-request'] })
    } catch (error) {
        return ctx.badRequest(error['message']);
    }
}

async function findOne(ctx) {
    try {
        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
        if (!allow) return ctx.badRequest(message)
        const transferQuery = await strapi.services['transfer-request'].findOne({ "student.generated_school_id": ctx.params.id })
        if (transferQuery.to_institution.id == principal_data.institution.id) {
            return sanitizeEntity(transferQuery, { model: strapi.models['transfer-request'] })
        }
        else {
            return ("This principal does not own this student")
        }

    } catch (error) {
        return ctx.badRequest(error['message']);
    }
}

async function acceptTransfer(ctx) {
    try {
        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
        if (!allow) return ctx.badRequest(message)
        console.log(principal_data.institution)
        const transferQuery = await strapi.services['transfer-request'].findOne({ "student.generated_school_id": ctx.params.id })
        if (transferQuery.from_institution.id == principal_data.institution.id) {
            const id = transferQuery.student.id
            const compTrsfer = await strapi.services.student.update({ id }, { "institution": transferQuery.to_institution })
            if (compTrsfer) {
                await strapi.services['transfer-request'].delete({ field_id: transferQuery.field_id })
            }
            return sanitizeEntity(compTrsfer, { model: strapi.models.student })
        }
        else {
            return ("This principal does not own this student")
        }

    } catch (error) {
        return ctx.badRequest(error['message']);
    }
}

module.exports = { create, find, findOne, acceptTransfer };
