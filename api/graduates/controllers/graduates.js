'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 * 
 * 
 */

const inst= "institution"
async function create(ctx) {
    try {
        const tkn = await strapi.plugins['users-permissions'].services.jwt.getToken(ctx)
        const id = ctx.request.body['student_id']
        let principal_data = await strapi.services.principal.findOne({ "base_user": tkn['id'] })
        if (!principal_data.institution) return ({ "status": "failed", "message": "Principal has no institution yet" })

        let std_data = await strapi.services.student.findOne({ id })
        if (!std_data) return ctx.badRequest("Student does not exist")
        if (principal_data['institution']['id'] != std_data['institution']['id']) return ctx.badRequest({ message: "Principal does not own this student" })
        if (std_data['is_owing']) return ctx.badRequest("Students owing cannot graduate")
        std_data = await strapi.services.student.update({ id }, { "study_status": "graduated" })
        const { full_name, phone_number, generated_school_id } = std_data
        const institution = std_data['institution']['id']
        let graduated_student = await strapi.services.graduates.create({ full_name, phone_number, generated_school_id, institution })
        if (!graduated_student) return ctx.badRequest("The student was not added to the graduated list")
        await strapi.services.student.delete({ id })
        return sanitizeEntity(graduated_student, { model: strapi.models.graduates })
    } catch (error) {
        return ctx.badRequest({ "message": error['message'] })
    }
}

async function find(ctx){
    let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
    if (!allow) return ctx.badRequest(message)
    const all_graduates = await strapi.services.graduates.find({"institution":principal_data[inst]['id'],"_limit":-1})
    return sanitizeEntity(all_graduates,{model:strapi.models.graduates})
}
async function findOne(ctx){
    const{name} = ctx.params
    let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
    if (!allow) return ctx.badRequest(message)
    const grad = await strapi.services.graduates.findOne({"full_name":name})
    return sanitizeEntity(grad,{model:strapi.models.graduates})
}
module.exports = { create ,find,findOne};
