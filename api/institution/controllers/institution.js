'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

async function create(ctx) {
    let entity = {}
    try {
        if (ctx.is('multipart')) {
            const { data, files } = parseMultipartData(ctx);
            entity['entity'] = await strapi.services.institution.create(data, { files });
        } else {
            let principal_data = await strapi.services.principal.get_principal_data(ctx)
            if (!principal_data) return ctx.badRequest("Principal does not exist")
            if (principal_data.institution) return ctx.badRequest("Principal can pnly have one institution")
            ctx.request.body['principal'] = principal_data['id']
            entity = await strapi.services.institution.create(ctx.request.body)
        }
        return sanitizeEntity(entity, { model: strapi.models.institution });
    } catch (error) {
        return ctx.badRequest(error['message']);
    }

}

module.exports = { create };