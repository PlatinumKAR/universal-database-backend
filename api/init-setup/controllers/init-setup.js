'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */
async function setup(ctx) {
    try {


        const plugin = await strapi.plugins['users-permissions'].services.userspermissions.getPlugins('en')
        const permissions = await strapi.plugins['users-permissions'].services.userspermissions.getActions(plugin)
        permissions['application']['controllers']['student']['find']['enabled'] = true
        permissions['application']['controllers']['student']['findOne']['enabled'] = true
        permissions['application']['controllers']['student']['count']['enabled'] = true
        permissions['application']['controllers']['student']['create']['enabled'] = true
        permissions['application']['controllers']['student']['update']['enabled'] = true
        permissions['application']['controllers']['student']['delete']['enabled'] = true
        permissions['application']['controllers']['student']['setOwing']['enabled'] = true
        permissions['application']['controllers']['student']['setAllOwing']['enabled'] = true
        permissions['application']['controllers']['student']['setAllPaid']['enabled'] = true
        permissions['application']['controllers']['institution']['create']['enabled'] = true
        permissions['application']['controllers']['graduates']['create']['enabled'] = true
        permissions['application']['controllers']['graduates']['findOne']['enabled'] = true
        permissions['application']['controllers']['graduates']['find']['enabled'] = true
        permissions['application']['controllers']['subscription']['findOne']['enabled'] = true
        permissions['application']['controllers']['student']['createBulk']['enabled'] = true
        permissions['application']['controllers']['payment-link']['create']['enabled'] = true
        const role = { name: "Principal", description: "testing", permissions, users: [] }

         await strapi.plugins['users-permissions'].services.userspermissions.createRole(role)

        await strapi.query('permission', 'users-permissions').update({ 'controller': "principal", "role": 1, "action": "create" }, { 'enabled': true })


        await strapi.query('permission', 'users-permissions').update({ 'controller': "principal", "role": 3, "action": "findme" }, { 'enabled': true })

        await strapi.query('permission', 'users-permissions').update({ 'controller': "product", "role": 2, "action": "find" }, { 'enabled': true })
        return ("successful")
    } catch (error) {
        ctx.badRequest("not successfull", error['message'])
    }


}
module.exports = { setup };
