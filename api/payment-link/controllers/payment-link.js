'use strict';
const axios = require('axios')
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */


async function create(ctx) {
    try {
        if (ctx.is('multipart')) {
            const { data, files } = parseMultipartData(ctx);
        } else {
            const { amount,redirect_url} = ctx.request.body
            let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
            if (!allow) return ctx.badRequest(message)
            const institution_id = principal_data.institution.id
            const postData = {
                tx_ref:`${institution_id}-${principal_data.id}-${Date.now()}`,
                amount,
                "currency": "GHS",
                redirect_url,
                "payment_options": "mobilemoneyghana",
                "meta": {
                    consumer_id:institution_id,
                    "consumer_mac": "92a3-912ba-1192a"
                },
                "customer": {
                    "email": principal_data.institution.email,
                    "phonenumber": principal_data.institution.phone_number,
                    "name": principal_data.institution.name
                },
    
            }
            const tkn = process.env.FLUTTERWAVEKEY

            const config = { headers: { Authorization: `Bearer ${tkn}`, "Content-Type": "application/json" }, }

            const { data } = await axios.post('https://api.flutterwave.com/v3/payments', postData, config);


            console.log(data);
            return (data)
        }
    } catch (error) {
        return ctx.badRequest(error['message']);
    }

}
module.exports = { create };
