'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

async function create(ctx) {
    let entity = {}
    try {
        let tkn = await strapi.plugins['users-permissions'].services.jwt.getToken(ctx)
        if (ctx.is('multipart')) {
            const { data, files } = parseMultipartData(ctx);
            entity['entity'] = await strapi.services.principal.create(data, { files });
        } else {
                ctx.request.body['base_user'] = tkn['id']
                entity['entity'] = await strapi.services.principal.create(ctx.request.body)

                await strapi.query('user', 'users-permissions').update({id:tkn['id']}, {"role":3})
        }
        return sanitizeEntity(entity, { model: strapi.models.principal });
    } catch (error) {
        return ctx.badRequest(error['message']);
    }
}


async function findme(ctx){
    try {
        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
        if (!allow) return ctx.badRequest(message)
        return sanitizeEntity(principal_data, { model: strapi.models.principal })
        
    } catch (error) {
        return ctx.badRequest(error['message']);
    }
}

module.exports = { create , findme};