'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */


async function principal_checks(ctx) {
    let message = ""
    let allow = true;
    const principal_data = await get_principal_data(ctx)
    if (!principal_data) {message = "Principal does not exist"; allow = false}
    if (!principal_data.institution){ message = "Principal has no institution yet"; allow = false}
    return { allow, message, principal_data }
}


async function get_principal_data(ctx) {
    const tkn = await strapi.plugins['users-permissions'].services.jwt.getToken(ctx)
    let principal_data = await strapi.services.principal.findOne({ "base_user": tkn['id'] })
    return principal_data
}
module.exports = { principal_checks, get_principal_data };
