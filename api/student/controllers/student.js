'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
var XLSX = require('xlsx')

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */
const is_owing = "is_owing"
const inst = 'institution'


async function create(ctx) {
    let entity = {}
    try {

        if (ctx.is('multipart')) {
            const { data, files } = parseMultipartData(ctx);
            entity['entity'] = await strapi.services.student.create(data, { files });
        } else {
            let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)

            if (!allow) return ctx.badRequest(message)
            const institution_id = principal_data.institution.id
            ctx.request.body['institution'] = institution_id
            ctx.request.body['generated_school_id'] = await strapi.services.student.genstudId(ctx.request.body['full_name'])
            entity['entity'] = await strapi.services.student.create(ctx.request.body)

        }
        return sanitizeEntity(entity, { model: strapi.models.student });
        // return entity['entity'].map(entity => sanitizeEntity(entity, { model: strapi.models.student }));
    } catch (error) {
        return ctx.badRequest(error['message']);
    }




}



async function setOwing(ctx) {
    try {
        const { id } = ctx.params
        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)

        if (!allow) return ctx.badRequest(message)
        let stud_data = await strapi.services.student.findOne({ id})
        let is_owing_status = stud_data[is_owing]
        let updated_student = await strapi.services.student.flipOwingService(is_owing_status, id)

        return sanitizeEntity(updated_student, { model: strapi.models.student })

    } catch (error) {
        return ctx.badRequest(error['message']);
    }

}




async function setAllOwing(ctx) {
    try {
        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
        let updated_records = []
        if (!allow) return ctx.badRequest(message)
        let all_stud_data = await strapi.services.student.find({ "institution": principal_data[inst]['id'],"_limit":-1 })
        for (let i = 0; i <= all_stud_data.length - 1; i++) {
            updated_records.push(await strapi.services.student.setOwingService(all_stud_data[i]['id']))
            console.log(updated_records[i]['id'], updated_records[i][is_owing])
        }


        return sanitizeEntity(updated_records, { model: strapi.models.student })
    } catch (error) {
        return ctx.badRequest(error['message']);
    }
}


async function setAllPaid(ctx) {
    try {
        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
        let updated_records = []
        if (!allow) return ctx.badRequest(message)
        let all_stud_data = await strapi.services.student.find({ "institution": principal_data[inst]['id'],"_limit":-1 })
        for (let i = 0; i <= all_stud_data.length - 1; i++) {
            updated_records.push(await strapi.services.student.setPaidService(all_stud_data[i]['id']))
            console.log(updated_records[i]['id'], updated_records[i][is_owing])
        }


        return sanitizeEntity(updated_records, { model: strapi.models.student })
    } catch (error) {
        return ctx.badRequest(error['message']);
    }
}


async function find(ctx) {
    try {
        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
        if (!allow) return ctx.badRequest(message)
        const principal_student = await strapi.services.student.find({ "institution": principal_data['institution']['id'],"_limit":-1 })
        return sanitizeEntity(principal_student, { model: strapi.models.student })

    } catch (error) {
        return ctx.badRequest(error['message']);
    }
}


async function createBulk(ctx) {
    try {

        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
        if (!allow) return ctx.badRequest(message)
        if (ctx.is('multipart')) {
            const { files } = parseMultipartData(ctx)
            const samp = files.excel
            var workbook = XLSX.readFile(samp.path);
            var sheet_name_list = workbook.SheetNames;
            var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
            const all_records = xlData
            let sucRec = []

            for (let i = 0; i <= all_records.length - 1; i++) {
                let record = all_records[i]
                const full_name = record['Full Name'].toLowerCase()
                const generated_school_id = await strapi.services.student.genstudId(full_name)
                const phone_number = record['Phone number'].toString()
                const institution = principal_data[inst]['id']
                const is_owing = record['Is Owing']
                const study_status = record['Study Status'].toLowerCase()
                sucRec.push(await strapi.services.student.create({ full_name, phone_number, generated_school_id, institution, is_owing, study_status }))
            }

            return ({ "message": "successfully created all students", "data": sucRec })
        }
        else {
            return ctx.badRequest("You must use multipart");
        }
    } catch (error) {
        console.log(error)
        return ctx.badRequest(error['message']);
    }
}


async function findOne(ctx) {
    try {
        let { allow, message, principal_data } = await strapi.services.principal.principal_checks(ctx)
        if (!allow) return ctx.badRequest(message)
        const principal_student = await strapi.services.student.find({ "full_name": ctx.params.name })
        return sanitizeEntity(principal_student, { model: strapi.models.student })

    } catch (error) {
        return ctx.badRequest(error['message']);
    }
}





module.exports = { create, setOwing, find, createBulk, findOne, setAllOwing, setAllPaid};
