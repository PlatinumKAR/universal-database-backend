'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */


async function genstudId(full_name){
    let splitNam = full_name.split(" ")
    let initials = splitNam[0][0]+splitNam[splitNam.length-1][0]
    let rndNum = Math.floor(100000 +Math.random()*900000)
    let generated_school_id = initials+rndNum
    const student = await strapi.services.student.findOne({generated_school_id})
    if(student){
        genstudId(full_name)
    }else{
        return generated_school_id
    }
    
}


async function flipOwingService(status,id){
    let is_owing_status = status
    is_owing_status = !is_owing_status
   
    let updated_student = await strapi.services.student.update({ id }, { "is_owing": is_owing_status })
    return updated_student
}

async function setOwingService(id){   
    let updated_student = await strapi.services.student.update({ id }, { "is_owing": true })
    return updated_student
}

async function setPaidService(id){  
    let updated_student = await strapi.services.student.update({ id }, { "is_owing": false })
    return updated_student
}


module.exports = {genstudId, setOwingService, flipOwingService,setPaidService};
