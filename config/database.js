module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'sqlite',
        filename: env('DATABASE_FILENAME', '.tmp/data.db'),
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});

// module.exports = ({ env }) => ({
//   defaultConnection: 'default',
//   connections: {
//     default: {
//       connector: 'bookshelf',
//       settings: {
//         client: 'postgres',
//         host: env('DATABASE_HOST', process.env.DATABASE_HOST),
//         port: env.int('DATABASE_PORT', process.env.DATABASE_PORT),
//         database: env('DATABASE_NAME', process.env.DATABASE_NAME),
//         username: env('DATABASE_USERNAME', process.env.DATABASE_USERNAME),
//         password: env('DATABASE_PASSWORD', process.env.DATABASE_PASSWORD),
//         schema: env('DATABASE_SCHEMA', 'public'), // Not Required
//         ssl: {
//           rejectUnauthorized: env.bool('DATABASE_SSL_SELF', false), // For self-signed certificates
//         },
//       },
//       options: {},
//     },
//   },
// });

 

 